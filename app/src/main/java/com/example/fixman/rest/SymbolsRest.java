package com.example.fixman.rest;

import android.util.Log;

import com.example.fixman.logic.JsonToSymbolsDeserializer;
import com.example.fixman.model.Constants;
import com.example.fixman.model.Symbols;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Dictionary;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SymbolsRest {
    private static final String TAG = SymbolsRest.class.getSimpleName();

    public static void init(){
        if(Symbols.isEmpty()){
            new Thread(initializer).start();
        }
    }

    private static final Runnable initializer = () -> {
        Endpoints service = getSymbolsEndpoint();
        Call<Dictionary<String, String>> call = service.symbols(Constants.API_KEY);
        Log.d(TAG, call.toString());
        try {
            Response<Dictionary<String, String>> response = call.execute();
            Log.d(TAG, response.toString());
            if (response.isSuccessful()) {
                Symbols.init(response.body());
            } else {
                Log.d(TAG, response.message());
            }
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }
    };

    private static Endpoints getSymbolsEndpoint() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type type = new TypeToken<Dictionary<String, String>>(){}.getType();
        gsonBuilder.registerTypeAdapter(type, new JsonToSymbolsDeserializer());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://data.fixer.io/")
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build();
        return retrofit.create(Endpoints.class);
    }
}
