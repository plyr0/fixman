package com.example.fixman.rest;

import com.example.fixman.model.Item;

import java.util.Dictionary;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface Endpoints {
    @GET("api/{date}")
    Call<Item[]> historicalRates(@Path("date") String date, @Query("access_key") String access_key);

    @GET("api/latest")
    Call<Item[]> latestRates(@Query("access_key") String access_key);

    @GET("api/symbols")
    Call<Dictionary<String, String>> symbols(@Query("access_key") String access_key);
}
