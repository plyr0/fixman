package com.example.fixman.rest;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.fixman.model.Constants;
import com.example.fixman.model.Item;
import com.example.fixman.logic.JsonToItemsDeserializer;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Rest {

    public interface DataListener {
        void onDataUpdated(Item[] data, boolean success);
    }
    private static Endpoints service;
    private static final String TAG = Rest.class.getSimpleName();
    private static Call<Item[]> restCall;

    private Rest() {}

    public static void init() {
        if(service == null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(Item[].class, new JsonToItemsDeserializer());

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://data.fixer.io/")
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .build();
            service = retrofit.create(Endpoints.class);
        }
    }

    public static void requestHistoricalRates(String date, final DataListener listener) {
        if(restCall != null && restCall.isExecuted()){
            restCall.cancel();
        }
        restCall = service.historicalRates(date, Constants.API_KEY);
        Log.d(TAG, restCall.toString());
        restCall.enqueue(getCallback(listener));
    }

    public static void requestLatestRates(DataListener listener) {
        if(restCall != null && restCall.isExecuted()){
            restCall.cancel();
        }
        restCall = service.latestRates(Constants.API_KEY);
        Log.d(TAG, restCall.toString());
        restCall.enqueue(getCallback(listener));
    }

    private static Callback<Item[]> getCallback(final DataListener listener){
        return new Callback<Item[]>() {
            @Override
            public void onResponse(@NonNull Call<Item[]> call, @NonNull Response<Item[]> response) {
                if(response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, response.toString());
                    if(call == restCall) {
                        listener.onDataUpdated(response.body(), true);
                    }
                } else {
                    Log.d(TAG, response.toString());
                    if(call == restCall) {
                        Item[] r = {new Item(response.message())};
                        listener.onDataUpdated(r, false);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Item[]> call, @NonNull Throwable t) {
                Log.d(TAG, t.toString());
                if(call == restCall) {
                    Item[] r = {new Item(t.getMessage())};
                    listener.onDataUpdated(r, false);
                }
            }
        };
    }
}
