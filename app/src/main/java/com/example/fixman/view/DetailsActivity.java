package com.example.fixman.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.fixman.R;
import com.example.fixman.model.Item;
import com.example.fixman.model.Symbols;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Item item = (Item)getIntent().getSerializableExtra("item");

        TextView tv = findViewById(R.id.detailsTextViewDate);
        tv.setText(item.date);
        tv = findViewById(R.id.detailsTextViewValue);
        tv.setText(String.format("1 EUR = %s %s", item.currencyRating, item.currencyName));
        tv = findViewById(R.id.detailsTextViewCurrencyName);
        tv.setText(Symbols.get(item.currencyName));
    }
}
