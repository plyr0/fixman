package com.example.fixman.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.fixman.R;
import com.example.fixman.logic.DateConverter;
import com.example.fixman.logic.ItemRecyclerAdapter;
import com.example.fixman.model.DataItems;
import com.example.fixman.model.Item;
import com.example.fixman.rest.Rest;
import com.example.fixman.rest.SymbolsRest;

import java.io.Serializable;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private ItemRecyclerAdapter adapter;
    private final DataItems data = new DataItems();
    private LinearLayoutManager linearLayout;
    private RecyclerView recycler;


    private final View.OnClickListener recyclerClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            int itemPosition = recycler.getChildLayoutPosition(view);
            Item item = data.get(itemPosition);
            if(!item.isHeader) {
                Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
                intent.putExtra("item", item);
                startActivity(intent);
            }
        }
    };

    private final Rest.DataListener dataListener = new Rest.DataListener() {
        @Override
        public void onDataUpdated(Item[] data, boolean success) {
            adapter.stopUpdating();
            if(success){
                MainActivity.this.data.addAll(Arrays.asList(data));
                adapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getApplicationContext(), data[0].date, Toast.LENGTH_LONG).show();
            }
        }
    };

    private final RecyclerView.OnScrollListener recyclerScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if(adapter.isReady()) {
                if (linearLayout.findLastCompletelyVisibleItemPosition() >= linearLayout.getItemCount() - 1) {
                    updateData();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler = findViewById(R.id.recycler);
        linearLayout = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayout);
        adapter = new ItemRecyclerAdapter(data, recyclerClickListener);
        recycler.setAdapter(adapter);
        recycler.addOnScrollListener(recyclerScrollListener);

        SymbolsRest.init();
        Rest.init();

        if(savedInstanceState == null){
            updateData();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.load_more:
                updateData();
                return true;
            case R.id.about:
                new AlertDialog.Builder(this)
                        .setMessage("Mateusz Teklak\nplyr00@gmail.com")
                        .setCancelable(true)
                        .setPositiveButton("Zamknij", (dialog, id) -> dialog.cancel())
                        .create()
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("data", data);
        outState.putString("nextDate", DateConverter.format(data.getNextDate()));
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Serializable ser = savedInstanceState.getSerializable("data");
        if(ser != null){
            DataItems di = (DataItems)ser;
            data.clear();
            data.addAll(di.getAll());

        }
        String d = savedInstanceState.getString("nextDate");
        data.setNextDate(DateConverter.parse(d));
    }

    private void updateData() {
        adapter.startUpdating();
        if(data.getNextDate() == null){
            Rest.requestLatestRates(dataListener);
        } else {
            Rest.requestHistoricalRates(DateConverter.format(data.getNextDate()), dataListener);
        }
    }
}
