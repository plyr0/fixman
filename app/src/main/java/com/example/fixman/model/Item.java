package com.example.fixman.model;

import java.io.Serializable;

public class Item implements Serializable {
    public final String date;
    public final boolean isHeader;
    public final String currencyName;
    public final String currencyRating;

    public Item(String date) {
        this.date = date;
        this.isHeader = true;
        currencyName = currencyRating = "";
    }

    public Item(String date, String currencyName, String currencyRating) {
        this.date = date;
        this.isHeader = false;
        this.currencyName = currencyName;
        this.currencyRating = currencyRating;
    }

    @Override
    public String toString() {
        return "Item{" +
                "date='" + date + '\'' +
                ", isHeader=" + isHeader +
                ", currencyName='" + currencyName + '\'' +
                ", currencyRating='" + currencyRating + '\'' +
                '}';
    }
}
