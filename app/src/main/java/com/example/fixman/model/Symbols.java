package com.example.fixman.model;

import java.util.Dictionary;
import java.util.Hashtable;

public class Symbols {
    private static Dictionary<String, String> symbols = new Hashtable<>();

    public static String get(String key) {
        String val = symbols.get(key);
        return val == null ? "" : val;
    }

    public static void init(Dictionary<String, String> symbols) {
        if (Symbols.symbols.isEmpty()) {
            Symbols.symbols = symbols;
        }
    }

    public static boolean isEmpty(){
        return symbols.isEmpty();
    }
}
