package com.example.fixman.model;

import com.example.fixman.logic.DateConverter;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DataItems implements Serializable {
    private final LinkedList<Item> data = new LinkedList<>();
    private Date nextDate;

    public Item get(int itemPosition) {
        return data.get(itemPosition);
    }

    public void addAll(List<Item> items) {
        data.addAll(items);
        Date d = DateConverter.parse(items.get(0).date);
        nextDate = DateConverter.decrementDay(d);
    }

    public void add(Item item) {
        data.add(item);
    }

    public int size() {
        return data.size();
    }

    public void remove(int i) {
        data.remove(i);
    }

    public void clear() {
        data.clear();
    }

    public List<Item> getAll() {
        return data;
    }

    public Date getNextDate() {
        return nextDate;
    }

    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }
}
