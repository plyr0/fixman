package com.example.fixman.logic;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fixman.R;
import com.example.fixman.model.DataItems;
import com.example.fixman.model.Item;

public class ItemRecyclerAdapter extends RecyclerView.Adapter<ItemRecyclerAdapter.ViewHolder> {
    private final DataItems data;
    private final View.OnClickListener listener;
    private boolean isReady = true;

    public ItemRecyclerAdapter(DataItems data, View.OnClickListener listener) {
        this.data = data;
        this.listener = listener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView textView;
        ViewHolder(TextView v) {
            super(v);
            textView = v;
        }
    }

    @NonNull
    @Override
    public ItemRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView tv = (TextView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);
        tv.setOnClickListener(listener);
        return new ViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item it = data.get(position);
        TextView tv = holder.textView;
        if(it.isHeader){
            tv.setText(String.format("%s", it.date));
            tv.setTextSize(48);
            tv.setTypeface(null, Typeface.BOLD);
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            tv.setTextSize(24);
            tv.setText(String.format("%s : %s", it.currencyName, it.currencyRating));
            tv.setTypeface(null, Typeface.NORMAL);
            tv.setGravity(Gravity.NO_GRAVITY);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public boolean isReady() {
        return isReady;
    }

    public void startUpdating() {
        if (isReady) {
            isReady = false;
            data.add(new Item("Ładowanie..."));
            notifyItemInserted(data.size() - 1);
        }
    }
    public void stopUpdating() {
        if(!isReady){
            data.remove(data.size() - 1);
            notifyItemRemoved(data.size() - 1);
            isReady = true;
        }
    }
}
