package com.example.fixman.logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateConverter {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public static Date parse(String date){
        Date out = new Date();
        try {
            out = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return out;
    }

    public static String format(Date date){
        return sdf.format(date);
    }

    public static Date decrementDay(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -1);
        return c.getTime();
    }
}
