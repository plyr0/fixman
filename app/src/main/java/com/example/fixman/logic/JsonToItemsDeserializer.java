package com.example.fixman.logic;

import com.example.fixman.model.Item;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JsonToItemsDeserializer implements JsonDeserializer<Item[]> {

    @Override
    public Item[] deserialize (JsonElement jElement, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        JsonObject jObject = jElement.getAsJsonObject();
        List<Item> ret = new LinkedList<>();

        if(jObject.get("success").getAsBoolean()){
            String date = jObject.get("date").getAsString();
            ret.add(new Item(date));

            for(Map.Entry<String, JsonElement> i : jObject.get("rates").getAsJsonObject().entrySet()){
                String currency = i.getKey();
                String rate = i.getValue().getAsString();
                ret.add(new Item(date, currency, rate));
            }

        } else {
            String error = jObject.get("error").getAsJsonObject().get("info").getAsString();
            ret.add(new Item(error));
        }
        return ret.toArray(new Item[0]);
    }
}
