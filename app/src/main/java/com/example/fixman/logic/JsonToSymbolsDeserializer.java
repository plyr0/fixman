package com.example.fixman.logic;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

public class JsonToSymbolsDeserializer implements JsonDeserializer<Dictionary<String, String>> {
    private static final String TAG = JsonToSymbolsDeserializer.class.getSimpleName();

    @Override
    public Dictionary<String, String> deserialize(JsonElement json, Type typeOfT,
                                                  JsonDeserializationContext context) throws JsonParseException {
        Hashtable<String, String> ret = new Hashtable<>();
        JsonObject jObject = json.getAsJsonObject();
        if(jObject.get("success").getAsBoolean()){
            for(Map.Entry<String, JsonElement> i : jObject.get("symbols").getAsJsonObject().entrySet()){
                String currency = i.getKey();
                String name = i.getValue().getAsString();
                ret.put(currency, name);
            }
            return ret;
        } else {
            String error = jObject.get("error").getAsJsonObject().get("info").getAsString();
            Log.d(TAG, error);
            return null;
        }
    }
}
